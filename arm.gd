extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var freq = 5
var speed = 100
var timer = 1/freq

var bullet = preload("res://Bullet.tscn")
# Called when the node enters the scene tree for the first time.
func _process(delta):
	timer -= delta
	if timer <= 0:
		timer = 1/freq
		if Input.is_action_pressed("shoot"):
			var bullet_inst = bullet.instance()
			#bullet_inst.translation = self.translation
			#bullet_inst.linear_velocity = ($end.translation - self.translation) * speed

			bullet_inst.global_transform.origin = self.global_transform.origin
			bullet_inst.linear_velocity = -(self.global_transform.origin - $end.global_transform.origin) * speed
			get_tree().root.add_child(bullet_inst)
			






# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
